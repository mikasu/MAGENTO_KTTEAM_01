<?php
namespace KTteam\Test\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
* @codeCoverageIgnore
*/
class InstallSchema implements InstallSchemaInterface
    {
        /**
            * {@inheritdoc}
        * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
        */
        public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
        {
            /**
            * Create table 'module_test_table'
            */
             $table = $setup->getConnection()
            ->newTable($setup->getTable('module_test_table'))
                ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
                )
                ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Name'
                )
                 ->addColumn(
                     'description',
                     \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                     255,
                     ['nullable' => false, 'default' => ''],
                     'Description'
                 )
                 ->addColumn(
                     'status',
                     \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                     255,
                     ['nullable' => false, 'default' => ''],
                     'Status'
                 )->setComment("Module test table");
            $setup->getConnection()->createTable($table);
        }
    }
